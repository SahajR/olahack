package ola.sahajr.testing_api;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

public class MainActivity extends Activity {//implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String CLASS_NAME = MainActivity.class.getSimpleName();
    TextView t;
    Boolean IS_CAB_AVAILABLE = false;
    String FARE_ESTIMATE = "0";
    double latitude1 = 12.9667, longitude1 = 77.5667;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t = (TextView) findViewById(R.id.t1);


        if (isNetworkAvailable()) {
            new AvailabilityResponseAsyncTask().execute("sedan");
        }
    }

    //First parameter is car type. Returns if it is available
    private class AvailabilityResponseAsyncTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://sandbox-t.olacabs.com/v1/products?pickup_lat=" +
                            latitude1 + "&pickup_lng=" +
                            longitude1 + "&category=" + params[0])
                    .get()
                    .addHeader("x-app-token", "52e7f0ed8bec49a98293be1fc814fefc")
                    .addHeader("authorization", "527ad01fbd0d4f52935e606cc4626a6e")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                String body = response.body().string();
                org.json.JSONObject carDetails = new org.json.JSONObject(body);
                Log.i(CLASS_NAME, body);
                return carDetails.has("categories");
            } catch (java.io.IOException | JSONException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean param) {
            if (param) {
                IS_CAB_AVAILABLE = true;
                Toast.makeText(MainActivity.this, "Available", Toast.LENGTH_LONG).show();
            }

        }
    }

    //First param is Car type. Second is Destination name. Returns the estimated fare as a string.
    private class getFareEstimate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();

            try {
                Location destLoc = getDestinationLocation("Sadashiv Peth, Pune");
                Request request;
                request = new Request.Builder()
                        .url("http://sandbox-t.olacabs.com/v1/products?pickup_lat=" + latitude1
                                + "&pickup_lng=" + longitude1 +
                                "&drop_lat=" + destLoc.getLatitude() + "&drop_lng=" +
                                destLoc.getLongitude() + "&category=" + params[0])
                        .get()
                        .addHeader("authorization", "Bearer e65f7ee2185642f1abab5c26d94b70b8")
                        .addHeader("x-app-token", "52e7f0ed8bec49a98293be1fc814fefc")
                        .build();

                Response response = client.newCall(request).execute();
                org.json.JSONObject carDetails = new org.json.JSONObject(response.body().string());
                org.json.JSONObject rideEstimate = carDetails.getJSONObject("ride_estimate");

                if (carDetails.has("id"))
                    return Integer.toString(rideEstimate.getInt("amount_min")) + "up to" + Integer.toString(rideEstimate.getInt("amount_max"));
                else
                    return "UNAVAILABLE";
            } catch (java.io.IOException | JSONException e) {
                e.printStackTrace();
            }
            return "UNAVAILABLE";
        }

        @Override
        protected void onPostExecute(String param) {
            FARE_ESTIMATE = param;
        }
    }

    //Checks network connectivity
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //Does what it says
//    private Location getLastKnownLocation(){
//        return LocationServices.FusedLocationApi.getLastLocation(
//                mGoogleApiClient);
//    }

    //Returns the Location from name of the Destination
    private Location getDestinationLocation(String address) throws IOException {
        Location l = new Location("");
        Log.i(CLASS_NAME, "Location: " + l);
        Geocoder geocoder = new Geocoder(MainActivity.this);
        List<Address> addresses;
        addresses = geocoder.getFromLocationName(address, 1);
        if (addresses.size() > 0) {
            double latitude = addresses.get(0).getLatitude();
            double longitude = addresses.get(0).getLongitude();
            l.setLatitude(latitude);
            l.setLongitude(longitude);
        }
        return l;
    }

}
